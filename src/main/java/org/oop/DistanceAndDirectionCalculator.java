package org.oop;

public class DistanceAndDirectionCalculator {
    public static double distance(Point from, Point to) {
        return Point.calculateDistance(to.getX(),from.getX(),to.getY(),from.getY());

    }

    public static double direction(Point from, Point to) {
        return Point.calculateDirection(to.getX(),from.getX(),to.getY(),from.getY());

    }
}
