package org.oop;

public class Point {

    private double x;
    private double y;

    public static double calculateDistance(double x1, double x2, double y1, double y2) {
        double xDistance = x1 - x2;
        double yDistance = y1 - y2;
        return Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2));
    }

    public static double calculateDirection(double x1, double x2, double y1, double y2) {
        double xDistance = x1 - x2;
        double yDistance = y1 - y2;
        return Math.atan2(yDistance, xDistance);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

}
